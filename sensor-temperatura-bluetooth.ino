#include "DHT.h"

#define sensorTemperatura A1 // pino que estamos conectado
#define tipoSensor DHT11 // Define o tipo de sensor
int ledAlerta = 13;

DHT dht(sensorTemperatura, tipoSensor);

void setup() {
  Serial.begin(9600);
  dht.begin();
  pinMode(ledAlerta, OUTPUT);
}

void loop() {
  float temperatura = dht.readTemperature();
  if (isnan(temperatura)) {
    Serial.println("Não foi possível ler a temperatura");
  } else {
    Serial.print('#'); // marcador de inicio do dado p envio bluetooth
    Serial.print(temperatura);
    Serial.print('~'); // marcador fim do dado p envio bluetooth
    Serial.println();
    digitalWrite(ledAlerta, HIGH);
    delay(100);
    digitalWrite(ledAlerta, LOW);
    delay(5000);
  }
  digitalWrite(ledAlerta, LOW);
}
